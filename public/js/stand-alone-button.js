(function ($) {

  $.fn.filemanager = function (type, options) {
    type = type || 'file';

    this.on('click', function (e) {
      var h = screen.height;
      var w = screen.width;

      var route_prefix = (options && options.prefix) ? options.prefix : '/filemanager';
      var target_input = $('#' + $(this).data('input'));
      var target_thumbinput = null;
      if ($.trim($(this).data('thumb-input'))
        && $('#' + $(this).data('thumb-input')).length
      ) {
        target_thumbinput = $('#' + $(this).data('thumb-input'));
      }
      var target_preview = $('#' + $(this).data('preview'));

      window.open(route_prefix + '?type=' + type, 'FileManager', `width=${w},height=${h}`);
      window.SetUrl = function (items) {
        var file_path = items.map(function (item) {
          return item.url;
        }).join(',');

        // set the value of the desired input to image url
        target_input.val('').val(file_path).trigger('change');

        if (target_thumbinput) {
          var file_thumb_path = items.map(function (item) {
            return item.thumb_url;
          }).join(',');
          target_thumbinput.val('').val(file_thumb_path).trigger('change');
        }

        // clear previous preview
        target_preview.html('');

        // set or change the preview image src
        items.forEach(function (item) {
          target_preview.append(
            $('<img>').css({
              height: '150px',
              display: 'block'
            }).attr('src', item.thumb_url)
          );
        });

        // trigger change event
        target_preview.trigger('change');
      };
      return false;
    });
  }

  $("[data-upload='image']").filemanager('image');
  $("[data-upload='file']").filemanager('file');

})(jQuery);
