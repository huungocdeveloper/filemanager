This is the document of v1 version.

## Features
 * CKEditor and TinyMCE integration
 * Standalone button
 * Uploading validation
 * Cropping and resizing of images
 * Public and private folders for multi users
 * Customizable routes, middlewares, views, and folder path
 * Supports two types : files and images. Each type works in different directory.
 * Supported locales : ar, bg, de, el, en, es, fa, fr, it, he, hu, nl, pl, pt-BR, pt_PT, ro, ru, sv, tr, zh-CN, zh-TW

PRs are welcome!

## Screenshots
> Standalone button :

![Standalone button demo](https://gitlab.com/huungocdeveloper/filemanager/raw/master/docs/images/lfm01.png)

> Responsive design :

![RWD demo](https://gitlab.com/huungocdeveloper/filemanager/raw/master/docs/images/screenshots-v2.png)
  
