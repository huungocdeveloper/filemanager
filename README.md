<p align="center"><img src="https://gitlab.com/huungocdeveloper/filemanager/raw/master/docs/images/logo_vertical_colored.png"></p>

<p align="center">
  <a target="_blank" href="https://gitlab.com/huungocdeveloper/filemanager"><img src="https://img.shields.io/badge/stable-1.0.0-blue.svg"></a>
  <a target="_blank" href="https://badges.mit-license.org/"><img src="https://gitlab.com/huungocdeveloper/filemanager/raw/master/docs/images/license.png"></a>
  <a target="_blank" href="https://gitlab.com/huungocdeveloper/filemanager"><img src="https://img.shields.io/badge/unstable-dev--master-orange.svg"></a>
</p>

<p align="center">
  <a href="https://gitlab.com/huungocdeveloper/filemanager/-/blob/master/docs/index.md" target="_blank">Documents</a>
・
  <a href="https://gitlab.com/huungocdeveloper/filemanager/blob/master/docs/installation.md" target="_blank">Installation</a>
・
  <a href="https://gitlab.com/huungocdeveloper/filemanager/blob/master/docs/integration.md" target="_blank">Integration</a>
・
  <a href="https://gitlab.com/huungocdeveloper/filemanager/blob/master/docs/config.md" target="_blank">Config</a>
・
  <a href="https://gitlab.com/huungocdeveloper/filemanager/blob/master/docs/customization.md" target="_blank">Customization</a>
・
  <a href="https://gitlab.com/huungocdeveloper/filemanager/blob/master/docs/events.md" target="_blank">Events</a>
・
  <a href="https://gitlab.com/huungocdeveloper/filemanager/blob/master/docs/upgrade.md" target="_blank">Upgrade</a>
</p>

## v1.0 progress

🎉Finally v1.0.0 has been released! 🎉

These are primary features of this update:
* Redesigned RWD user interface.
* Supporting multiple files selection.
* Supporting cloud storages integration(with Laravel file system).
* Refactored code and increased test coverage.

Although it is not perfect enough, we decided to release it. Now users who want the above features won't need to install alpha versions anymore.

There are still some thing we need to do make this package better:
* [ ] Documents for v1.0.0
* [ ] Events should pass object instead of only file path
* [ ] Add more events for files and folders manipulation

We are also going to gain more integration like vue.js and Laravel Nova. Any PR is welcome!

![RWD demo](https://gitlab.com/huungocdeveloper/filemanager/-/raw/master/docs/images/screenshots-v2.png)

## License

[<img src="https://gitlab.com/huungocdeveloper/filemanager/raw/master/docs/images/license.png">](https://badges.mit-license.org/)

+ [MIT license](https://badges.mit-license.org/)
+ Copyright 2020 © [Huu Ngoc Developer](http://huungocblog.com/)
