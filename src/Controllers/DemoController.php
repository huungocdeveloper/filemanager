<?php

namespace HuungocDeveloper\FileManager\Controllers;

class DemoController extends LfmController
{
    public function index()
    {
        return view('filemanager::demo');
    }
}
