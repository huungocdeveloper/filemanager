<?php

namespace HuungocDeveloper\FileManager;

use Carbon\Carbon;
use Illuminate\Support\Str;

class LfmItem
{
    private $lfm;
    private $helper;
    private $isDirectory;
    private $mimeType = null;

    private $columns = ['name', 'url', 'time', 'icon', 'is_file', 'is_image', 'thumb_url'];
    public $attributes = [];

    /**
     * LfmItem constructor.
     *
     * @param LfmPath $lfm
     * @param Lfm $helper
     * @param bool $isDirectory
     */
    public function __construct(LfmPath $lfm, Lfm $helper, $isDirectory = false)
    {
        $this->lfm = $lfm->thumb(false);
        $this->helper = $helper;
        $this->isDirectory = $isDirectory;
    }

    /**
     * @param $var_name
     * @return mixed
     */
    public function __get($var_name)
    {
        if (!array_key_exists($var_name, $this->attributes)) {
            $function_name = Str::camel($var_name);
            $this->attributes[$var_name] = $this->$function_name();
        }

        return $this->attributes[$var_name];
    }

    /**
     * @return $this
     */
    public function fill()
    {
        foreach ($this->columns as $column) {
            $this->__get($column);
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function name()
    {
        return $this->lfm->getName();
    }

    /**
     * @param string $type
     * @return string|string[]
     */
    public function path($type = 'absolute')
    {
        return $this->lfm->path($type);
    }

    /**
     * @return bool
     */
    public function isDirectory()
    {
        return $this->isDirectory;
    }

    /**
     * @return bool
     */
    public function isFile()
    {
        return !$this->isDirectory();
    }

    /**
     * Check a file is image or not.
     *
     * @param mixed $file Real path of a file or instance of UploadedFile.
     * @return bool
     */
    public function isImage()
    {
        return $this->isFile() && Str::startsWith($this->mimeType(), 'image');
    }

    /**
     * Get mime type of a file.
     *
     * @param mixed $file Real path of a file or instance of UploadedFile.
     * @return string
     */
    public function mimeType()
    {
        if (is_null($this->mimeType)) {
            $this->mimeType = $this->lfm->mimeType();
        }

        return $this->mimeType;
    }

    /**
     * @return mixed
     */
    public function extension()
    {
        return $this->lfm->extension();
    }

    /**
     * @return mixed|string|string[]
     */
    public function url()
    {
        if ($this->isDirectory()) {
            return $this->lfm->path('working_dir');
        }

        return $this->lfm->url();
    }

    /**
     * @return string
     */
    public function size()
    {
        return $this->isFile() ? $this->humanFilesize($this->lfm->size()) : '';
    }

    /**
     * @return bool
     */
    public function time()
    {
        if (!$this->isDirectory()) {
            $datetime = $this->lfm->lastModified();

            return Carbon::parse($datetime)->format(config('lfm.datetime_format'));
        }

        return null;
    }

    /**
     * @return mixed|string|null
     */
    public function thumbUrl(): ?string
    {
        if ($this->isDirectory()) {
            return asset('vendor/' . Lfm::PACKAGE_NAME . '/img/folder.png');
        }

        if ($this->isImage()) {
            return $this->lfm->thumb($this->hasThumb())->url(true);
        }

        return null;
    }

    /**
     * @return mixed|string
     */
    public function icon()
    {
        if ($this->isDirectory()) {
            return 'fa-folder-o';
        }

        if ($this->isImage()) {
            return 'fa-image';
        }

        return $this->extension();
    }

    /**
     * @return array|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Translation\Translator|mixed|string|null
     */
    public function type()
    {
        if ($this->isDirectory()) {
            return trans(Lfm::PACKAGE_NAME . '::lfm.type-folder');
        }

        if ($this->isImage()) {
            return $this->mimeType();
        }

        return $this->helper->getFileType($this->extension());
    }

    /**
     * @return bool
     */
    public function hasThumb()
    {
        if (!$this->isImage()) {
            return false;
        }

        if (!$this->lfm->thumb()->exists()) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function shouldCreateThumb()
    {
        if (!$this->helper->config('should_create_thumbnails')) {
            return false;
        }

        if (!$this->isImage()) {
            return false;
        }

        if (in_array($this->mimeType(), ['image/gif', 'image/svg+xml'])) {
            return false;
        }

        return true;
    }

    /**
     * @return mixed
     */
    public function get()
    {
        return $this->lfm->get();
    }

    /**
     * Make file size readable.
     *
     * @param int $bytes File size in bytes.
     * @param int $decimals Decimals.
     * @return string
     */
    public function humanFilesize($bytes, $decimals = 2)
    {
        $size = ['B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
        $factor = floor((strlen($bytes) - 1) / 3);

        return sprintf("%.{$decimals}f %s", $bytes / pow(1024, $factor), @$size[$factor]);
    }
}
