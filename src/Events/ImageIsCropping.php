<?php

namespace HuungocDeveloper\FileManager\Events;

class ImageIsCropping
{
    private $path;

    public function __construct($path)
    {
        $this->path = $path;
    }

    /**
     * @return string
     */
    public function path()
    {
        return $this->path;
    }
}
